## Legacy SOAP Service of the Taxonomy

How to run application locally

```
./mvnw spring-boot:run
```

### How it is deployed

The following page explains how it is deployed: <https://confluence.arbetsformedlingen.se/pages/viewpage.action?pageId=200704829>

> Användare av Direktöverförda Annonser kan använda en taxonomi för att hämta yrkesbenämningar.
>
> https://api.arbetsformedlingen.se/taxonomi/v0/TaxonomiService.asmx
>
> Detta är en SOAP tjänst som JobTech driftar och finns på följande URL:
>
> https://taxonomy-soap-prod.jobtechdev.se
>
> En ompekning har skapats i F5 som omdirigerar trafik till den externa URL till den interna URL.

### Docker

```
podman build -t soap .

podman run -p 8080:8080 soap
```

### smoke test

``` shell
./mvnw spring-boot:run & 
./test/curl.sh 
```

## probe example

YAML-konfiguration for Kubernetes:
```
livenessProbe:
        exec:
          command:
            - sh
            - '-c'
            - >-
              curl -X POST http://localhost:8080/TaxonomiService.asmx -H
              'Content-Type: text/xml' -d '<soapenv:Envelope
              xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
              xmlns:urn="urn:ams.se:Taxonomy"> <soapenv:Header/>
              <soapenv:Body><urn:GetAllContinents><languageId>?</languageId></urn:GetAllContinents></soapenv:Body></soapenv:Envelope>'
        timeoutSeconds: 10
        periodSeconds: 20
        successThreshold: 1
        failureThreshold: 3
```
