#!/bin/bash
set -e

# Set host to the first argument if provided, otherwise use the default value
host="${1:-http://localhost:8080/TaxonomiService.asmx}"
echo "Using host: $host"

for test_file in test/soap_queries/*.xml; do
  test_file_name=${test_file##*/}
  test_name=${test_file_name%.xml}
  echo $test_name
  curl -v --request POST --header @test/curl_header --data @test/soap_queries/"$test_name".xml $host -o "$test_name"_response.xml

  # Cross-platform check for file size (non-empty check)
  if [ -s "$test_name"_response.xml ]; then
    result_size=$(wc -c < "$test_name"_response.xml)
    echo "Success $test_name received $result_size bytes";
  else
    echo "Failed $test_name";
    exit 1;
  fi
done

TIME=$(date +%s)
cat <<EOF > junit-soap-test-report.xml
<?xml version="1.0" encoding="UTF-8"?>
<testsuite name="CurlTests" tests="1" failures="0" time="$TIME">
  <properties>
    <property name="tests" value="1"/>
    <property name="assertions" value="10"/>
    <property name="failures" value="0"/>
    <property name="time" value="$TIME"/>
  </properties>
  <testcase classname="ServerAPITests" name="TestSuite" time="$TIME"/>
</testsuite>
EOF


