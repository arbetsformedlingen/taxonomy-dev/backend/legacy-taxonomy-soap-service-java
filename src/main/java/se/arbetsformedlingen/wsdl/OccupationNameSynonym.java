//
// This file was generated by the Eclipse Implementation of JAXB, v3.0.0 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.08.30 at 10:47:28 AM CEST 
//


package se.arbetsformedlingen.wsdl;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OccupationNameSynonym complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OccupationNameSynonym"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PopularSynonymID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SynonymTerm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OccupationNameID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="OccupationNameTerm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OccupationNameSynonym", namespace = "urn:ams.se:Taxonomy", propOrder = {
    "popularSynonymID",
    "synonymTerm",
    "occupationNameID",
    "occupationNameTerm"
})
public class OccupationNameSynonym {

    @XmlElement(name = "PopularSynonymID")
    protected int popularSynonymID;
    @XmlElement(name = "SynonymTerm")
    protected String synonymTerm;
    @XmlElement(name = "OccupationNameID")
    protected int occupationNameID;
    @XmlElement(name = "OccupationNameTerm")
    protected String occupationNameTerm;

    /**
     * Gets the value of the popularSynonymID property.
     * 
     */
    public int getPopularSynonymID() {
        return popularSynonymID;
    }

    /**
     * Sets the value of the popularSynonymID property.
     * 
     */
    public void setPopularSynonymID(int value) {
        this.popularSynonymID = value;
    }

    /**
     * Gets the value of the synonymTerm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSynonymTerm() {
        return synonymTerm;
    }

    /**
     * Sets the value of the synonymTerm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSynonymTerm(String value) {
        this.synonymTerm = value;
    }

    /**
     * Gets the value of the occupationNameID property.
     * 
     */
    public int getOccupationNameID() {
        return occupationNameID;
    }

    /**
     * Sets the value of the occupationNameID property.
     * 
     */
    public void setOccupationNameID(int value) {
        this.occupationNameID = value;
    }

    /**
     * Gets the value of the occupationNameTerm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupationNameTerm() {
        return occupationNameTerm;
    }

    /**
     * Sets the value of the occupationNameTerm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupationNameTerm(String value) {
        this.occupationNameTerm = value;
    }

}
