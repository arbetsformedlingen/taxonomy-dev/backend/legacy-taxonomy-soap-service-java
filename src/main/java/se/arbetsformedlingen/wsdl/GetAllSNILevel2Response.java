//
// This file was generated by the Eclipse Implementation of JAXB, v3.0.0 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.08.30 at 10:47:28 AM CEST 
//


package se.arbetsformedlingen.wsdl;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetAllSNILevel2Result" type="{urn:ams.se:Taxonomy}ArrayOfSNILevel2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllSNILevel2Result"
})
@XmlRootElement(name = "GetAllSNILevel2Response", namespace = "urn:ams.se:Taxonomy")
public class GetAllSNILevel2Response {

    @XmlElement(name = "GetAllSNILevel2Result")
    protected ArrayOfSNILevel2 getAllSNILevel2Result;

    /**
     * Gets the value of the getAllSNILevel2Result property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSNILevel2 }
     *     
     */
    public ArrayOfSNILevel2 getGetAllSNILevel2Result() {
        return getAllSNILevel2Result;
    }

    /**
     * Sets the value of the getAllSNILevel2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSNILevel2 }
     *     
     */
    public void setGetAllSNILevel2Result(ArrayOfSNILevel2 value) {
        this.getAllSNILevel2Result = value;
    }

}
