//
// This file was generated by the Eclipse Implementation of JAXB, v3.0.0 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.08.30 at 10:47:28 AM CEST 
//


package se.arbetsformedlingen.wsdl;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetAllMunicipalityHomePagesResult" type="{urn:ams.se:Taxonomy}ArrayOfMunicipalityHomePage" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllMunicipalityHomePagesResult"
})
@XmlRootElement(name = "GetAllMunicipalityHomePagesResponse", namespace = "urn:ams.se:Taxonomy")
public class GetAllMunicipalityHomePagesResponse {

    @XmlElement(name = "GetAllMunicipalityHomePagesResult")
    protected ArrayOfMunicipalityHomePage getAllMunicipalityHomePagesResult;

    /**
     * Gets the value of the getAllMunicipalityHomePagesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMunicipalityHomePage }
     *     
     */
    public ArrayOfMunicipalityHomePage getGetAllMunicipalityHomePagesResult() {
        return getAllMunicipalityHomePagesResult;
    }

    /**
     * Sets the value of the getAllMunicipalityHomePagesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMunicipalityHomePage }
     *     
     */
    public void setGetAllMunicipalityHomePagesResult(ArrayOfMunicipalityHomePage value) {
        this.getAllMunicipalityHomePagesResult = value;
    }

}
