//
// This file was generated by the Eclipse Implementation of JAXB, v3.0.0 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.08.30 at 10:47:28 AM CEST 
//


package se.arbetsformedlingen.wsdl;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OccupationExperienceYear complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OccupationExperienceYear"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OccupationExperienceYearID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ExperienceYearContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExperienceYearCandidate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OccupationExperienceYear", namespace = "urn:ams.se:Taxonomy", propOrder = {
    "occupationExperienceYearID",
    "experienceYearContact",
    "experienceYearCandidate"
})
public class OccupationExperienceYear {

    @XmlElement(name = "OccupationExperienceYearID")
    protected int occupationExperienceYearID;
    @XmlElement(name = "ExperienceYearContact")
    protected String experienceYearContact;
    @XmlElement(name = "ExperienceYearCandidate")
    protected String experienceYearCandidate;

    /**
     * Gets the value of the occupationExperienceYearID property.
     * 
     */
    public int getOccupationExperienceYearID() {
        return occupationExperienceYearID;
    }

    /**
     * Sets the value of the occupationExperienceYearID property.
     * 
     */
    public void setOccupationExperienceYearID(int value) {
        this.occupationExperienceYearID = value;
    }

    /**
     * Gets the value of the experienceYearContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExperienceYearContact() {
        return experienceYearContact;
    }

    /**
     * Sets the value of the experienceYearContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExperienceYearContact(String value) {
        this.experienceYearContact = value;
    }

    /**
     * Gets the value of the experienceYearCandidate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExperienceYearCandidate() {
        return experienceYearCandidate;
    }

    /**
     * Sets the value of the experienceYearCandidate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExperienceYearCandidate(String value) {
        this.experienceYearCandidate = value;
    }

}
