//
// This file was generated by the Eclipse Implementation of JAXB, v3.0.0 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.08.30 at 10:47:28 AM CEST 
//


package se.arbetsformedlingen.wsdl;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PostCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PostCode"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PostLocalityID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PostLocality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NationalNUTSLAU2Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="KeyAccount" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PostCode", namespace = "urn:ams.se:Taxonomy", propOrder = {
    "code",
    "postLocalityID",
    "postLocality",
    "nationalNUTSLAU2Code",
    "keyAccount"
})
public class PostCode {

    @XmlElement(name = "Code")
    protected String code;
    @XmlElement(name = "PostLocalityID")
    protected int postLocalityID;
    @XmlElement(name = "PostLocality")
    protected String postLocality;
    @XmlElement(name = "NationalNUTSLAU2Code")
    protected String nationalNUTSLAU2Code;
    @XmlElement(name = "KeyAccount")
    protected boolean keyAccount;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the postLocalityID property.
     * 
     */
    public int getPostLocalityID() {
        return postLocalityID;
    }

    /**
     * Sets the value of the postLocalityID property.
     * 
     */
    public void setPostLocalityID(int value) {
        this.postLocalityID = value;
    }

    /**
     * Gets the value of the postLocality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostLocality() {
        return postLocality;
    }

    /**
     * Sets the value of the postLocality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostLocality(String value) {
        this.postLocality = value;
    }

    /**
     * Gets the value of the nationalNUTSLAU2Code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalNUTSLAU2Code() {
        return nationalNUTSLAU2Code;
    }

    /**
     * Sets the value of the nationalNUTSLAU2Code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalNUTSLAU2Code(String value) {
        this.nationalNUTSLAU2Code = value;
    }

    /**
     * Gets the value of the keyAccount property.
     * 
     */
    public boolean isKeyAccount() {
        return keyAccount;
    }

    /**
     * Sets the value of the keyAccount property.
     * 
     */
    public void setKeyAccount(boolean value) {
        this.keyAccount = value;
    }

}
