//
// This file was generated by the Eclipse Implementation of JAXB, v3.0.0 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.08.30 at 10:47:28 AM CEST 
//


package se.arbetsformedlingen.wsdl;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SUNLevel3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SUNLevel3"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SUNLevel3ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SUNLevel3Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Term" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SUNLevel2ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SUNLevel3", namespace = "urn:ams.se:Taxonomy", propOrder = {
    "sunLevel3ID",
    "sunLevel3Code",
    "term",
    "sunLevel2ID"
})
public class SUNLevel3 {

    @XmlElement(name = "SUNLevel3ID")
    protected int sunLevel3ID;
    @XmlElement(name = "SUNLevel3Code")
    protected String sunLevel3Code;
    @XmlElement(name = "Term")
    protected String term;
    @XmlElement(name = "SUNLevel2ID")
    protected int sunLevel2ID;

    /**
     * Gets the value of the sunLevel3ID property.
     * 
     */
    public int getSUNLevel3ID() {
        return sunLevel3ID;
    }

    /**
     * Sets the value of the sunLevel3ID property.
     * 
     */
    public void setSUNLevel3ID(int value) {
        this.sunLevel3ID = value;
    }

    /**
     * Gets the value of the sunLevel3Code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUNLevel3Code() {
        return sunLevel3Code;
    }

    /**
     * Sets the value of the sunLevel3Code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUNLevel3Code(String value) {
        this.sunLevel3Code = value;
    }

    /**
     * Gets the value of the term property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerm() {
        return term;
    }

    /**
     * Sets the value of the term property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerm(String value) {
        this.term = value;
    }

    /**
     * Gets the value of the sunLevel2ID property.
     * 
     */
    public int getSUNLevel2ID() {
        return sunLevel2ID;
    }

    /**
     * Sets the value of the sunLevel2ID property.
     * 
     */
    public void setSUNLevel2ID(int value) {
        this.sunLevel2ID = value;
    }

}
