//
// This file was generated by the Eclipse Implementation of JAXB, v3.0.0 
// See https://eclipse-ee4j.github.io/jaxb-ri 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2023.08.30 at 10:47:28 AM CEST 
//


package se.arbetsformedlingen.wsdl;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SUNField2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SUNField2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SUNField2ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="SUNField2Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Term" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SUNField1ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SUNField2", namespace = "urn:ams.se:Taxonomy", propOrder = {
    "sunField2ID",
    "sunField2Code",
    "term",
    "sunField1ID"
})
public class SUNField2 {

    @XmlElement(name = "SUNField2ID")
    protected int sunField2ID;
    @XmlElement(name = "SUNField2Code")
    protected String sunField2Code;
    @XmlElement(name = "Term")
    protected String term;
    @XmlElement(name = "SUNField1ID")
    protected int sunField1ID;

    /**
     * Gets the value of the sunField2ID property.
     * 
     */
    public int getSUNField2ID() {
        return sunField2ID;
    }

    /**
     * Sets the value of the sunField2ID property.
     * 
     */
    public void setSUNField2ID(int value) {
        this.sunField2ID = value;
    }

    /**
     * Gets the value of the sunField2Code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUNField2Code() {
        return sunField2Code;
    }

    /**
     * Sets the value of the sunField2Code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUNField2Code(String value) {
        this.sunField2Code = value;
    }

    /**
     * Gets the value of the term property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerm() {
        return term;
    }

    /**
     * Sets the value of the term property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerm(String value) {
        this.term = value;
    }

    /**
     * Gets the value of the sunField1ID property.
     * 
     */
    public int getSUNField1ID() {
        return sunField1ID;
    }

    /**
     * Sets the value of the sunField1ID property.
     * 
     */
    public void setSUNField1ID(int value) {
        this.sunField1ID = value;
    }

}
