package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllWageTypes;
        import se.arbetsformedlingen.wsdl.GetAllWageTypesResponse;

@Endpoint
public class WageTypeEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private WageTypeRepository wageTypeRepository;

    @Autowired
    public WageTypeEndpoint(WageTypeRepository wageTypeRepository) {
        this.wageTypeRepository = wageTypeRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllWageTypes")
    @ResponsePayload
    public GetAllWageTypesResponse getAllWageTypes(@RequestPayload GetAllWageTypes request) {
        return wageTypeRepository.getAllWageTypes();
    }

}
