package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllLocaleGroups;
        import se.arbetsformedlingen.wsdl.GetAllLocaleGroupsResponse;

@Endpoint
public class LocaleGroupEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private LocaleGroupRepository localeGroupRepository;

    @Autowired
    public LocaleGroupEndpoint(LocaleGroupRepository localeGroupRepository) {
        this.localeGroupRepository = localeGroupRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllLocaleGroups")
    @ResponsePayload
    public GetAllLocaleGroupsResponse getAllLocaleGroups(@RequestPayload GetAllLocaleGroups request) {
        return localeGroupRepository.getAllLocaleGroups();
    }

}
