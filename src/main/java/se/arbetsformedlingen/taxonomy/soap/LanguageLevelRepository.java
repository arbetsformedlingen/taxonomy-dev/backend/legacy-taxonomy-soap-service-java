package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class LanguageLevelRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllLanguageLevelsResponse response = new GetAllLanguageLevelsResponse();


    @PostConstruct
    public void initData() {

        ArrayOfLanguageLevel array = new ArrayOfLanguageLevel();
        response.setGetAllLanguageLevelsResult(array);

        Resource file = resourceLoader.getResource("classpath:LanguageLevel.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                LanguageLevel o = new LanguageLevel();

                o.setLanguageLevelID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 


                array.getLanguageLevel().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllLanguageLevelsResponse getAllLanguageLevels() {
        return response;
    }
}
