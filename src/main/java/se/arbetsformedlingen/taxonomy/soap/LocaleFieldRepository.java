package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class LocaleFieldRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllLocaleFieldsResponse response = new GetAllLocaleFieldsResponse();


    @PostConstruct
    public void initData() {

        ArrayOfLocaleField array = new ArrayOfLocaleField();
        response.setGetAllLocaleFieldsResult(array);

        Resource file = resourceLoader.getResource("classpath:LocaleField.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                LocaleField o = new LocaleField();

                o.setLocaleFieldID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 
o.setDescription(split[2]); 


                array.getLocaleField().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllLocaleFieldsResponse getAllLocaleFields() {
        return response;
    }
}
