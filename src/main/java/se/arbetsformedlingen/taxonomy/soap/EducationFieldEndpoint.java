package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllEducationFields;
        import se.arbetsformedlingen.wsdl.GetAllEducationFieldsResponse;

@Endpoint
public class EducationFieldEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private EducationFieldRepository educationFieldRepository;

    @Autowired
    public EducationFieldEndpoint(EducationFieldRepository educationFieldRepository) {
        this.educationFieldRepository = educationFieldRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllEducationFields")
    @ResponsePayload
    public GetAllEducationFieldsResponse getAllEducationFields(@RequestPayload GetAllEducationFields request) {
        return educationFieldRepository.getAllEducationFields();
    }

}
