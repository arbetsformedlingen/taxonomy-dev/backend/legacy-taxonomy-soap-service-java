package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class EmploymentDurationRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllEmploymentDurationsResponse response = new GetAllEmploymentDurationsResponse();


    @PostConstruct
    public void initData() {

        ArrayOfEmploymentDuration array = new ArrayOfEmploymentDuration();
        response.setGetAllEmploymentDurationsResult(array);

        Resource file = resourceLoader.getResource("classpath:EmploymentDuration.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                EmploymentDuration o = new EmploymentDuration();

                o.setEmploymentDurationID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 
o.setEURESCode(split[2]); 
o.setSortOrder(Integer.valueOf(split[3])); 


                array.getEmploymentDuration().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllEmploymentDurationsResponse getAllEmploymentDurations() {
        return response;
    }
}
