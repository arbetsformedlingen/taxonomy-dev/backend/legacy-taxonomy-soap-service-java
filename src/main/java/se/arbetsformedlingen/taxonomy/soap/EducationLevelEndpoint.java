package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllEducationLevels;
        import se.arbetsformedlingen.wsdl.GetAllEducationLevelsResponse;

@Endpoint
public class EducationLevelEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private EducationLevelRepository educationLevelRepository;

    @Autowired
    public EducationLevelEndpoint(EducationLevelRepository educationLevelRepository) {
        this.educationLevelRepository = educationLevelRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllEducationLevels")
    @ResponsePayload
    public GetAllEducationLevelsResponse getAllEducationLevels(@RequestPayload GetAllEducationLevels request) {
        return educationLevelRepository.getAllEducationLevels();
    }

}
