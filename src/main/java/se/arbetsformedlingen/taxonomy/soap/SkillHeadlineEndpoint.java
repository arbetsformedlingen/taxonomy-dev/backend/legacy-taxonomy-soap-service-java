package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllSkillHeadlines;
        import se.arbetsformedlingen.wsdl.GetAllSkillHeadlinesResponse;

@Endpoint
public class SkillHeadlineEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private SkillHeadlineRepository skillHeadlineRepository;

    @Autowired
    public SkillHeadlineEndpoint(SkillHeadlineRepository skillHeadlineRepository) {
        this.skillHeadlineRepository = skillHeadlineRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllSkillHeadlines")
    @ResponsePayload
    public GetAllSkillHeadlinesResponse getAllSkillHeadlines(@RequestPayload GetAllSkillHeadlines request) {
        return skillHeadlineRepository.getAllSkillHeadlines();
    }

}
