package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class AIDOccupationNameRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllAIDOccupationNamesResponse response = new GetAllAIDOccupationNamesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfAIDOccupationName array = new ArrayOfAIDOccupationName();
        response.setGetAllAIDOccupationNamesResult(array);

        Resource file = resourceLoader.getResource("classpath:AIDOccupationName.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                AIDOccupationName o = new AIDOccupationName();

                o.setLabel(split[0]);
o.setOccupationNameID(Integer.valueOf(split[1]));


                array.getAIDOccupationName().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllAIDOccupationNamesResponse getAllAIDOccupationNames() {
        return response;
    }
}
