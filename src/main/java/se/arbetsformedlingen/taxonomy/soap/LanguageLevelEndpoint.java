package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllLanguageLevels;
        import se.arbetsformedlingen.wsdl.GetAllLanguageLevelsResponse;

@Endpoint
public class LanguageLevelEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private LanguageLevelRepository languageLevelRepository;

    @Autowired
    public LanguageLevelEndpoint(LanguageLevelRepository languageLevelRepository) {
        this.languageLevelRepository = languageLevelRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllLanguageLevels")
    @ResponsePayload
    public GetAllLanguageLevelsResponse getAllLanguageLevels(@RequestPayload GetAllLanguageLevels request) {
        return languageLevelRepository.getAllLanguageLevels();
    }

}
