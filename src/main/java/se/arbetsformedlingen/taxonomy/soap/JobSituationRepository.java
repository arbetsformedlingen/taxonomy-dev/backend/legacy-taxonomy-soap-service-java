package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class JobSituationRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllJobSituationsResponse response = new GetAllJobSituationsResponse();


    @PostConstruct
    public void initData() {

        ArrayOfJobSituation array = new ArrayOfJobSituation();
        response.setGetAllJobSituationsResult(array);

        Resource file = resourceLoader.getResource("classpath:JobSituation.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                JobSituation o = new JobSituation();

                o.setJobSituationAISID(split[0]);
o.setJobSituationAIS(split[1]);
o.setTerm(split[2]); 
o.setSortOrder(Integer.valueOf(split[3])); 


                array.getJobSituation().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllJobSituationsResponse getAllJobSituations() {
        return response;
    }
}
