package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllContinents;
        import se.arbetsformedlingen.wsdl.GetAllContinentsResponse;

@Endpoint
public class ContinentEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private ContinentRepository continentRepository;

    @Autowired
    public ContinentEndpoint(ContinentRepository continentRepository) {
        this.continentRepository = continentRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllContinents")
    @ResponsePayload
    public GetAllContinentsResponse getAllContinents(@RequestPayload GetAllContinents request) {
        return continentRepository.getAllContinents();
    }

}
