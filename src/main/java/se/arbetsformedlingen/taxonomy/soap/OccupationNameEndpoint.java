package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllOccupationNames;
        import se.arbetsformedlingen.wsdl.GetAllOccupationNamesResponse;

@Endpoint
public class OccupationNameEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private OccupationNameRepository occupationNameRepository;

    @Autowired
    public OccupationNameEndpoint(OccupationNameRepository occupationNameRepository) {
        this.occupationNameRepository = occupationNameRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllOccupationNames")
    @ResponsePayload
    public GetAllOccupationNamesResponse getAllOccupationNames(@RequestPayload GetAllOccupationNames request) {
        return occupationNameRepository.getAllOccupationNames();
    }

}
