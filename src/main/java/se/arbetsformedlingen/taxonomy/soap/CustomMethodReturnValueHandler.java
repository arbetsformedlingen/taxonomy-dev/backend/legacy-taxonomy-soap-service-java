package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.core.MethodParameter;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.adapter.method.MethodReturnValueHandler;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import javax.xml.namespace.QName;
import jakarta.xml.soap.*;

public class CustomMethodReturnValueHandler implements MethodReturnValueHandler {


    private static final String DEFAULT_NS = "xmlns:soap";
    private static final String SOAP_ENV_NAMESPACE = "http://schemas.xmlsoap.org/soap/envelope/";
    private static final String PREFERRED_PREFIX = "soap";
    private static final String HEADER_LOCAL_NAME = "Header";
    private static final String BODY_LOCAL_NAME = "Body";
    private static final String FAULT_LOCAL_NAME = "Fault";

    @Override
    public boolean supportsReturnType(MethodParameter returnType) {

        System.out.println("returnType = " + returnType);
        return true; // var false
    }

    @Override
    public void handleReturnValue(MessageContext messageContext, MethodParameter returnType, Object returnValue) throws Exception {

        System.out.println("AGBHINAEBAAPNIBAPIONBRB \n \n AGJNWGNRPGNRGPEOING  \n is this working?");


        supportsReturnType(returnType);

        SaajSoapMessage response = (SaajSoapMessage) messageContext.getResponse();
        alterSoapEnvelope(response);
    }

    private void alterSoapEnvelope(SaajSoapMessage soapResponse) {
        try {
            SOAPMessage soapMessage = soapResponse.getSaajMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();
            SOAPEnvelope envelope = soapPart.getEnvelope();
            SOAPHeader header = soapMessage.getSOAPHeader();


            SOAPBody body = soapMessage.getSOAPBody();
            SOAPFault fault = body.getFault();
            envelope.removeNamespaceDeclaration(envelope.getPrefix());

            //<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">

            // behover bytas ut for version 1.2
            // <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">

            //System.out.println("---------------------------------\n\n");
            // System.out.println(header);

            if(header instanceof com.sun.xml.messaging.saaj.soap.ver1_2.Header1_2Impl){

                envelope.addNamespaceDeclaration(PREFERRED_PREFIX, "http://www.w3.org/2003/05/soap-envelope");

                // <soap:Envelope xmlns:soap=http://www.w3.org/2003/05/soap-envelope

            } else {
                envelope.addNamespaceDeclaration(PREFERRED_PREFIX, SOAP_ENV_NAMESPACE);
            }

            envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
            envelope.setPrefix(PREFERRED_PREFIX);
            header.setPrefix(PREFERRED_PREFIX);
            body.setPrefix(PREFERRED_PREFIX);

            SOAPBodyElement firstSoapBodyElement = (SOAPBodyElement) body.getFirstChild();

            Name elementName = firstSoapBodyElement.getElementName();
            QName qname = new QName(elementName.getLocalName());

            //    SOAPElement sOAPElement = (SOAPElement) qname;
            //    sOAPElement.addNamespaceDeclaration("banan", "apelsing");

            body.removeContents();
            body.addBodyElement(qname);
//xmlns="urn:ams.se:Taxonomy"
            QName qname2 = new QName("banan");

            ((SOAPBodyElement) body.getFirstChild()).addAttribute(qname2,"aperlsin");

            body.getFirstChild().appendChild(firstSoapBodyElement.getFirstChild());

            if (fault != null) {
                fault.setPrefix(PREFERRED_PREFIX);
            }

            soapMessage.setProperty(SOAPMessage.WRITE_XML_DECLARATION, Boolean.TRUE.toString());



            soapMessage.saveChanges();

        } catch (SOAPException e) {
            e.printStackTrace();
        }
    }
}
