package se.arbetsformedlingen.taxonomy.soap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import jakarta.xml.soap.*;

public class CustomEndpointInterceptor implements org.springframework.ws.server.EndpointInterceptor {

    Logger logger = LoggerFactory.getLogger(CustomEndpointInterceptor.class);

    private static String baseLogLine(MessageContext messageContext, String methodName) {
	return "msg " + System.identityHashCode(messageContext) + " " + methodName;
    }
    
    @Override
    public boolean handleRequest(MessageContext messageContext, Object endpoint) throws Exception {
	logger.info(baseLogLine(messageContext, "handleRequest") + ": " + endpoint.toString());
        return true;
    }

    private static final String DEFAULT_NS = "xmlns:soap";
    private static final String SOAP_ENV_NAMESPACE = "http://schemas.xmlsoap.org/soap/envelope/";
    private static final String PREFERRED_PREFIX = "soap";
    private static final String HEADER_LOCAL_NAME = "Header";
    private static final String BODY_LOCAL_NAME = "Body";
    private static final String FAULT_LOCAL_NAME = "Fault";

    private void alterSoapEnvelope(SaajSoapMessage soapResponse) {
        try {
            SOAPMessage soapMessage = soapResponse.getSaajMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();
            SOAPEnvelope envelope = soapPart.getEnvelope();
            SOAPHeader header = soapMessage.getSOAPHeader();

            soapMessage.getSOAPHeader().detachNode();

            SOAPBody body = soapMessage.getSOAPBody();
            SOAPFault fault = body.getFault();
            envelope.removeNamespaceDeclaration(envelope.getPrefix());

            if (header instanceof com.sun.xml.messaging.saaj.soap.ver1_2.Header1_2Impl) {

                envelope.addNamespaceDeclaration(PREFERRED_PREFIX, "http://www.w3.org/2003/05/soap-envelope");

                // <soap:Envelope xmlns:soap=http://www.w3.org/2003/05/soap-envelope

            } else {
                envelope.addNamespaceDeclaration(PREFERRED_PREFIX, SOAP_ENV_NAMESPACE);
            }

            envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
            envelope.setPrefix(PREFERRED_PREFIX);
            header.setPrefix(PREFERRED_PREFIX);
            body.setPrefix(PREFERRED_PREFIX);

            if (fault != null) {
                fault.setPrefix(PREFERRED_PREFIX);
            }

            soapMessage.setProperty(SOAPMessage.WRITE_XML_DECLARATION, Boolean.TRUE.toString());
            soapMessage.saveChanges();

        } catch (SOAPException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean handleResponse(MessageContext messageContext, Object endpoint) throws Exception {
        SaajSoapMessage response = (SaajSoapMessage) messageContext.getResponse();
        alterSoapEnvelope(response);
	logger.info(baseLogLine(messageContext, "handleResponse"));
        return true;
    }

    @Override
    public boolean handleFault(MessageContext messageContext, Object endpoint) throws Exception {
	String base = baseLogLine(messageContext, "handleFault");
	try {
	    SaajSoapMessage response = (SaajSoapMessage) messageContext.getResponse();
	    logger.info(base + ": " + response.getFaultReason());
	} catch (Exception e) {
	    logger.info(base + ": Failed to get reason");
	}
        return true;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Object endpoint, Exception ex) throws Exception {

    }
}
