package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class ContinentRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllContinentsResponse response = new GetAllContinentsResponse();


    @PostConstruct
    public void initData() {

        ArrayOfContinent array = new ArrayOfContinent();
        response.setGetAllContinentsResult(array);

        Resource file = resourceLoader.getResource("classpath:Continent.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                Continent o = new Continent();

                o.setContinentID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 


                array.getContinent().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllContinentsResponse getAllContinents() {
        return response;
    }
}
