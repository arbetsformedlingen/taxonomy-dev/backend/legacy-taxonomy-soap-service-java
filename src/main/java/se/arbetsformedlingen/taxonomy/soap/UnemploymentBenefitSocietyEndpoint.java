package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllUnemploymentBenefitSocieties;
        import se.arbetsformedlingen.wsdl.GetAllUnemploymentBenefitSocietiesResponse;

@Endpoint
public class UnemploymentBenefitSocietyEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private UnemploymentBenefitSocietyRepository unemploymentBenefitSocietyRepository;

    @Autowired
    public UnemploymentBenefitSocietyEndpoint(UnemploymentBenefitSocietyRepository unemploymentBenefitSocietyRepository) {
        this.unemploymentBenefitSocietyRepository = unemploymentBenefitSocietyRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllUnemploymentBenefitSocieties")
    @ResponsePayload
    public GetAllUnemploymentBenefitSocietiesResponse getAllUnemploymentBenefitSocieties(@RequestPayload GetAllUnemploymentBenefitSocieties request) {
        return unemploymentBenefitSocietyRepository.getAllUnemploymentBenefitSocieties();
    }

}
