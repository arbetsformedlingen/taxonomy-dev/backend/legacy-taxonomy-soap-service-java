package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllLocaleGroupSkills;
        import se.arbetsformedlingen.wsdl.GetAllLocaleGroupSkillsResponse;

@Endpoint
public class LocaleGroupSkillEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private LocaleGroupSkillRepository localeGroupSkillRepository;

    @Autowired
    public LocaleGroupSkillEndpoint(LocaleGroupSkillRepository localeGroupSkillRepository) {
        this.localeGroupSkillRepository = localeGroupSkillRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllLocaleGroupSkills")
    @ResponsePayload
    public GetAllLocaleGroupSkillsResponse getAllLocaleGroupSkills(@RequestPayload GetAllLocaleGroupSkills request) {
        return localeGroupSkillRepository.getAllLocaleGroupSkills();
    }

}
