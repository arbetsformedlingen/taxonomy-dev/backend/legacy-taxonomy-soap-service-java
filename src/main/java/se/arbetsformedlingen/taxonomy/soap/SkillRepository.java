package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class SkillRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllSkillsResponse response = new GetAllSkillsResponse();

    @PostConstruct
    public void initData() {

        ArrayOfSkill array = new ArrayOfSkill();
        response.setGetAllSkillsResult(array);

        Resource file = resourceLoader.getResource("classpath:Skill.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines(Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                Skill o = new Skill();

                // o.setSkillHeadline(split[0]);
                o.setSkillID(Integer.valueOf(split[1]));
                o.setTerm(split[2]);

                array.getSkill().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public GetAllSkillsResponse getAllSkills() {
        return response;
    }
}
