package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class LocaleGroupRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllLocaleGroupsResponse response = new GetAllLocaleGroupsResponse();

    @PostConstruct
    public void initData() {

        ArrayOfLocaleGroup array = new ArrayOfLocaleGroup();
        response.setGetAllLocaleGroupsResult(array);

        Resource file = resourceLoader.getResource("classpath:LocaleGroup.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines(Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                LocaleGroup o = new LocaleGroup();

                o.setLocaleGroupID(Integer.valueOf(split[0]));
                o.setLocaleCode(split[1]);
                o.setTerm(split[2]);
                o.setDescription(split[3]);
                o.setLocaleFieldID(Integer.valueOf(split[4]));
                o.setLocaleLevel3Code(split[5]);

                array.getLocaleGroup().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public GetAllLocaleGroupsResponse getAllLocaleGroups() {
        return response;
    }
}
