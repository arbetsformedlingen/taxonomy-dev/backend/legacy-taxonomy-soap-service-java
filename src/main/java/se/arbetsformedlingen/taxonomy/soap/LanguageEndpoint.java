package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllLanguages;
        import se.arbetsformedlingen.wsdl.GetAllLanguagesResponse;

@Endpoint
public class LanguageEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private LanguageRepository languageRepository;

    @Autowired
    public LanguageEndpoint(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllLanguages")
    @ResponsePayload
    public GetAllLanguagesResponse getAllLanguages(@RequestPayload GetAllLanguages request) {
        return languageRepository.getAllLanguages();
    }

}
