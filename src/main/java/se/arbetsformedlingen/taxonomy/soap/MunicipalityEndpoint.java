package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllMunicipalities;
        import se.arbetsformedlingen.wsdl.GetAllMunicipalitiesResponse;

@Endpoint
public class MunicipalityEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private MunicipalityRepository municipalityRepository;

    @Autowired
    public MunicipalityEndpoint(MunicipalityRepository municipalityRepository) {
        this.municipalityRepository = municipalityRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllMunicipalities")
    @ResponsePayload
    public GetAllMunicipalitiesResponse getAllMunicipalities(@RequestPayload GetAllMunicipalities request) {
        return municipalityRepository.getAllMunicipalities();
    }

}
