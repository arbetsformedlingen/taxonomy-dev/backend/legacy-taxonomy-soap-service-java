package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllJobSituations;
        import se.arbetsformedlingen.wsdl.GetAllJobSituationsResponse;

@Endpoint
public class JobSituationEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private JobSituationRepository jobSituationRepository;

    @Autowired
    public JobSituationEndpoint(JobSituationRepository jobSituationRepository) {
        this.jobSituationRepository = jobSituationRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllJobSituations")
    @ResponsePayload
    public GetAllJobSituationsResponse getAllJobSituations(@RequestPayload GetAllJobSituations request) {
        return jobSituationRepository.getAllJobSituations();
    }

}
