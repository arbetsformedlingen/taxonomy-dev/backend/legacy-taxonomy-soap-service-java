package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllWorkTimeExtents;
        import se.arbetsformedlingen.wsdl.GetAllWorkTimeExtentsResponse;

@Endpoint
public class WorkTimeExtentEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private WorkTimeExtentRepository workTimeExtentRepository;

    @Autowired
    public WorkTimeExtentEndpoint(WorkTimeExtentRepository workTimeExtentRepository) {
        this.workTimeExtentRepository = workTimeExtentRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllWorkTimeExtents")
    @ResponsePayload
    public GetAllWorkTimeExtentsResponse getAllWorkTimeExtents(@RequestPayload GetAllWorkTimeExtents request) {
        return workTimeExtentRepository.getAllWorkTimeExtents();
    }

}
