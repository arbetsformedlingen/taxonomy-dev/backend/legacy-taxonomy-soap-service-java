package se.arbetsformedlingen.taxonomy.soap;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.ws.server.endpoint.annotation.Endpoint;
        import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
        import org.springframework.ws.server.endpoint.annotation.RequestPayload;
        import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

        import se.arbetsformedlingen.wsdl.GetAllDrivingLicences;
        import se.arbetsformedlingen.wsdl.GetAllDrivingLicencesResponse;

@Endpoint
public class DrivingLicenceEndpoint {
    private static final String NAMESPACE_URI = "urn:ams.se:Taxonomy";



    private DrivingLicenceRepository drivingLicenceRepository;

    @Autowired
    public DrivingLicenceEndpoint(DrivingLicenceRepository drivingLicenceRepository) {
        this.drivingLicenceRepository = drivingLicenceRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllDrivingLicences")
    @ResponsePayload
    public GetAllDrivingLicencesResponse getAllDrivingLicences(@RequestPayload GetAllDrivingLicences request) {
        return drivingLicenceRepository.getAllDrivingLicences();
    }

}
