package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class EmploymentTypeRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllEmploymentTypesResponse response = new GetAllEmploymentTypesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfEmploymentType array = new ArrayOfEmploymentType();
        response.setGetAllEmploymentTypesResult(array);

        Resource file = resourceLoader.getResource("classpath:EmploymentType.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                EmploymentType o = new EmploymentType();

                o.setEmploymentTypeID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 
o.setSortOrder(Integer.valueOf(split[2])); 


                array.getEmploymentType().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllEmploymentTypesResponse getAllEmploymentTypes() {
        return response;
    }
}
