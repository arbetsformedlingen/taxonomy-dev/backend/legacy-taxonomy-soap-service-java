package se.arbetsformedlingen.taxonomy.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.wsdl.*;

import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Component
public class DrivingLicenceRepository {

    @Autowired
    ResourceLoader resourceLoader;

    private static final GetAllDrivingLicencesResponse response = new GetAllDrivingLicencesResponse();


    @PostConstruct
    public void initData() {

        ArrayOfDrivingLicence array = new ArrayOfDrivingLicence();
        response.setGetAllDrivingLicencesResult(array);

        Resource file = resourceLoader.getResource("classpath:DrivingLicence.csv");

        try {
            System.out.println(file.getFile().exists());

            Stream<String> lines = Files.lines( Paths.get(file.getFile().toURI()));

            Consumer<String> addData = line -> {
                String[] split = line.split(";");

                DrivingLicence o = new DrivingLicence();

                o.setDrivingLicenceID(Integer.valueOf(split[0])); 
o.setTerm(split[1]); 
o.setDescription(split[2]); 
o.setDrivingLicenceCode(split[3]); 
o.setSortOrder(split[4]);


                array.getDrivingLicence().add(o);

            };

            lines.forEach(addData);
            lines.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public GetAllDrivingLicencesResponse getAllDrivingLicences() {
        return response;
    }
}
