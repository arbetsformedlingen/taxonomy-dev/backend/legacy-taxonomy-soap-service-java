FROM eclipse-temurin:18.0.2.1_1-jdk-focal as build
WORKDIR application

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

RUN ./mvnw  install -DskipTests &&\
    cp /application/target/*.jar app.jar &&\
    java -Djarmode=layertools -jar app.jar extract

FROM docker.io/library/eclipse-temurin:21.0.2_13-jdk-jammy

RUN apt-get update && apt-get install -y curl

WORKDIR application
COPY --from=build application/dependencies/ \
                  application/spring-boot-loader/ \
                  application/snapshot-dependencies/ \
                  application/application/ ./

EXPOSE 8080

ENTRYPOINT ["java", "org.springframework.boot.loader.launch.JarLauncher"]
